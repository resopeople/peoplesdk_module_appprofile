<?php

use people_sdk\app_profile\requisition\request\info\factory\model\AppProfileConfigSndInfoFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'requisition' => [
            'request' => [
                'snd_info_factory' => [
                    'config' => [
                        'snd_info_factory' => [
                            ['snd_info_factory' => 'people_app_profile_requisition_request_snd_info_factory']
                        ]
                    ]
                ]
            ]
        ],

        'app_profile' => [
            'requisition' => [
                'request' => [
                    // Application profile requisition request sending information factory
                    'snd_info_factory' => [
                        /**
                         * Configuration array format:
                         * @see AppProfileConfigSndInfoFactory configuration format.
                         */
                        'config' => [
                            'app_profile_role_support_type' => 'header',
                            'app_profile_role_perm_full_update_config_key' => 'people_app_profile_role_perm_full_update',
                            'app_profile_role_role_full_update_config_key' => 'people_app_profile_role_role_full_update'
                        ]
                    ]
                ]
            ]
        ]
    ]
);