<?php

use people_sdk\app_profile\requisition\request\info\factory\model\AppProfileConfigSndInfoFactory;



return array(
    // Application profile requisition request sending information services
    // ******************************************************************************

    'people_app_profile_requisition_request_snd_info_factory' => [
        'source' => AppProfileConfigSndInfoFactory::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_config'],
            ['type' => 'config', 'value' => 'people/app_profile/requisition/request/snd_info_factory/config']
        ],
        'option' => [
            'shared' => true
        ]
    ]
);