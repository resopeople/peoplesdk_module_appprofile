<?php
/**
 * Description :
 * This class allows to define app module bootstrap class.
 * App module bootstrap allows to boot app module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_app_profile\app\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\module_app_profile\app\library\ConstApp;



class AppBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Application profile entity factory instance.
     * @var AppProfileEntityFactory
     */
    protected $objAppProfileEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param AppProfileEntityFactory $objAppProfileEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        AppProfileEntityFactory $objAppProfileEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objAppProfileEntityFactory = $objAppProfileEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstApp::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set application profile entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'app_profile', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objAppProfileEntityFactory->getTabConfig(), $tabConfig);
            $this->objAppProfileEntityFactory->setTabConfig($tabConfig);
        };
    }



}