<?php

use people_sdk\module_app_profile\app\boot\AppBootstrap;



return array(
    'people_app_profile_app_bootstrap' => [
        'call' => [
            'class_path_pattern' => AppBootstrap::class . ':boot'
        ]
    ]
);