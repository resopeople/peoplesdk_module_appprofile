<?php

use people_sdk\app_profile\app\model\AppProfileEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'app_profile' => [
            // Application profile entity factory
            'factory' => [
                /**
                 * Configuration array format:
                 * @see AppProfileEntityFactory configuration format.
                 */
                'config' => [
                    'select_entity_require' => true,
                    'select_entity_create_require' => true,
                    'select_entity_collection_set_require' => true
                ],

                /**
                 * Application profile attribute value entity factory execution configuration array format:
                 * @see AppProfileEntityFactory::setTabAppProfileAttrValueEntityFactoryExecConfig() configuration format.
                 */
                'app_profile_attribute_value_factory_execution_config' => [],

                /**
                 * Subject permission entity factory execution configuration array format:
                 * @see AppProfileEntityFactory::setTabSubjPermEntityFactoryExecConfig() configuration format.
                 */
                'subject_permission_factory_execution_config' => [],

                /**
                 * Role entity factory execution configuration array format:
                 * @see AppProfileEntityFactory::setTabRoleEntityFactoryExecConfig() configuration format.
                 */
                'role_factory_execution_config' => []
            ],

            // Application profile entity repository
            'repository' => [
                /**
                 * Boundary format:
                 * @var string
                 */
                'boundary' => ''
            ]
        ]
    ]
);