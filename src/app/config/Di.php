<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\app_profile\attribute\provider\model\AppProfileAttrProvider;
use people_sdk\app_profile\app\attribute\value\model\AppProfileAttrValueEntityFactory;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\app_profile\app\model\repository\AppProfileEntitySimpleRepository;
use people_sdk\app_profile\app\model\repository\AppProfileEntityMultiRepository;
use people_sdk\app_profile\app\model\repository\AppProfileEntityMultiCollectionRepository;
use people_sdk\module_app_profile\attribute\helper\AppProfileAttrProviderHelper;
use people_sdk\module_app_profile\app\helper\AppProfileFactoryHelper;



return array(
    // Application profile attribute value entity services
    // ******************************************************************************

    'people_app_profile_attr_value_entity_factory' => [
        'source' => AppProfileAttrValueEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'class', 'value' => AppProfileAttrProvider::class]
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Application profile entity services
    // ******************************************************************************

    'people_app_profile_entity_factory' => [
        'source' => AppProfileEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'mixed', 'value' => null],
            ['type' => 'dependency', 'value' => 'people_app_profile_attr_value_entity_factory'],
            ['type' => 'config', 'value' => 'people/app_profile/factory/app_profile_attribute_value_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/app_profile/factory/subject_permission_factory_execution_config'],
            ['type' => 'config', 'value' => 'people/app_profile/factory/role_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_app_profile_entity_simple_repository' => [
        'source' => AppProfileEntitySimpleRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class],
            ['type' => 'config', 'value' => 'people/app_profile/repository/boundary']
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_app_profile_entity_multi_repository' => [
        'source' => AppProfileEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class],
            ['type' => 'config', 'value' => 'people/app_profile/repository/boundary']
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_app_profile_entity_multi_collection_repository' => [
        'source' => AppProfileEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_app_profile_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_app_profile_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Application profile helper services
    // ******************************************************************************

    'people_app_profile_factory_helper' => [
        'source' => AppProfileFactoryHelper::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_app_profile_entity_factory'],
            ['type' => 'class', 'value' => AppProfileAttrProviderHelper::class]
        ],
        'option' => [
            'shared' => true
        ]
    ]
);