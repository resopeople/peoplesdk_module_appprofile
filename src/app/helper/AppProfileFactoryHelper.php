<?php
/**
 * Description :
 * This class allows to define application profile factory helper class.
 * Application profile factory helper allows to provide features,
 * for application profile entity factory.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_app_profile\app\helper;

use liberty_code\library\bean\model\DefaultBean;

use liberty_code\library\crypto\library\ToolBoxHash;
use people_sdk\app_profile\app\model\AppProfileEntityCollection;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\module_app_profile\attribute\helper\AppProfileAttrProviderHelper;



class AppProfileFactoryHelper extends DefaultBean
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application profile entity factory instance.
     * @var AppProfileEntityFactory
     */
    protected $objAppProfileEntityFactory;



    /**
     * DI: Application profile attribute provider helper instance.
     * @var AppProfileAttrProviderHelper
     */
    protected $objAppProfileAttrProviderHelper;



    /**
     * Associative array of application profile entity collection instances, used for schema.
     * @var AppProfileEntityCollection[]
     */
    protected $tabAppProfileEntityCollection;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AppProfileEntityFactory $objAppProfileEntityFactory
     * @param AppProfileAttrProviderHelper $objAppProfileAttrProviderHelper
     */
    public function __construct(
        AppProfileEntityFactory $objAppProfileEntityFactory,
        AppProfileAttrProviderHelper $objAppProfileAttrProviderHelper
    )
    {
        // Init properties
        $this->objAppProfileEntityFactory = $objAppProfileEntityFactory;
        $this->objAppProfileAttrProviderHelper = $objAppProfileAttrProviderHelper;
        $this->tabAppProfileEntityCollection = array();

        // Call parent constructor
        parent::__construct();
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Load application profile attributes, to get specified schemas,
     * available for application profile entity factory.
     * Return true if success, false if an error occurs.
     *
     * Application profile attribute entity collection repository execution configuration array format:
     * @see AppProfileAttrProviderHelper::loadSchema()
     * application profile attribute entity collection repository execution configuration array format.
     *
     * @param boolean $boolSchemaRequired = false
     * @param boolean $boolSchemaPrivateGetRequired = false
     * @param boolean $boolSchemaPrivateUpdateRequired = false
     * @param boolean $boolSchemaPublicGetRequired = false
     * @param boolean $boolSchemaPublicExtendGetRequired = false
     * @param null|array $tabAppProfileAttrEntityCollectionRepoExecConfig = null
     * @return boolean
     */
    public function loadSchema(
        $boolSchemaRequired = false,
        $boolSchemaPrivateGetRequired = false,
        $boolSchemaPrivateUpdateRequired = false,
        $boolSchemaPublicGetRequired = false,
        $boolSchemaPublicExtendGetRequired = false,
        array $tabAppProfileAttrEntityCollectionRepoExecConfig = null
    )
    {
        // Init provider
        $result = $this->objAppProfileAttrProviderHelper->loadSchema(
            $boolSchemaRequired,
            $boolSchemaPrivateGetRequired,
            $boolSchemaPrivateUpdateRequired,
            $boolSchemaPublicGetRequired,
            $boolSchemaPublicExtendGetRequired,
            $tabAppProfileAttrEntityCollectionRepoExecConfig
        );

        // Get application profile entity collection
        $strKey = ToolBoxHash::getStrHash(array(
            $boolSchemaRequired,
            $boolSchemaPrivateGetRequired,
            $boolSchemaPrivateUpdateRequired,
            $boolSchemaPublicGetRequired,
            $boolSchemaPublicExtendGetRequired
        ));
        if(!array_key_exists($strKey, $this->tabAppProfileEntityCollection))
        {
            $this->tabAppProfileEntityCollection[$strKey] = new AppProfileEntityCollection();
        }
        $objAppProfileEntityCollection = $this->tabAppProfileEntityCollection[$strKey];

        // Init factory
        $this->objAppProfileEntityFactory->setObjEntityCollection($objAppProfileEntityCollection);

        // Return result
        return $result;
    }



}