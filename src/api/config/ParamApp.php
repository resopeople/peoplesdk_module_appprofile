<?php

use people_sdk\app_profile\api\model\AppProfileApiKeyEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'app_profile' => [
            'api_key' => [
                // Application profile API key entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see AppProfileApiKeyEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * Application profile entity factory execution configuration array format:
                     * @see AppProfileApiKeyEntityFactory::setTabAppProfileEntityFactoryExecConfig() configuration format.
                     */
                    'app_profile_factory_execution_config' => []
                ]
            ]
        ]
    ]
);