<?php

use people_sdk\module_app_profile\api\boot\ApiBootstrap;



return array(
    'people_app_profile_api_bootstrap' => [
        'call' => [
            'class_path_pattern' => ApiBootstrap::class . ':boot'
        ]
    ]
);