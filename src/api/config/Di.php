<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\app_profile\api\model\AppProfileApiKeyEntityCollection;
use people_sdk\app_profile\api\model\AppProfileApiKeyEntityFactory;
use people_sdk\app_profile\api\model\repository\AppProfileApiKeyEntitySimpleRepository;
use people_sdk\app_profile\api\model\repository\AppProfileApiKeyEntityMultiRepository;
use people_sdk\app_profile\api\model\repository\AppProfileApiKeyEntityMultiCollectionRepository;



return array(
    // Application profile API key entity services
    // ******************************************************************************

    'people_app_profile_api_key_entity_collection' => [
        'source' => AppProfileApiKeyEntityCollection::class
    ],

    'people_app_profile_api_key_entity_factory_collection' => [
        'source' => AppProfileApiKeyEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_app_profile_api_key_entity_factory' => [
        'source' => AppProfileApiKeyEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_app_profile_api_key_entity_factory_collection'],
            ['type' => 'class', 'value' => AppProfileEntityFactory::class],
            ['type' => 'config', 'value' => 'people/app_profile/api_key/factory/app_profile_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_app_profile_api_key_entity_simple_repository' => [
        'source' => AppProfileApiKeyEntitySimpleRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_app_profile_api_key_entity_multi_repository' => [
        'source' => AppProfileApiKeyEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_app_profile_api_key_entity_multi_collection_repository' => [
        'source' => AppProfileApiKeyEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_app_profile_api_key_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_app_profile_api_key_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    AppProfileApiKeyEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_app_profile_api_key_entity_collection']
    ]
);