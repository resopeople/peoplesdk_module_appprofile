<?php

use liberty_code\register\register\table\model\DefaultTableRegister;
use liberty_code\cache\repository\format\model\FormatRepository;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'app_profile' => [
            // Application profile cache configuration
            'cache' => [
                'register' => [
                    /**
                     * Configuration array format:
                     * @see DefaultTableRegister configuration format.
                     */
                    'config' => []
                ],

                /**
                 * Configuration array format:
                 * @see FormatRepository configuration format.
                 */
                'config' => [
                    'key_pattern' => 'people-app-profile-%s',
                    'key_regexp_select' => '#people\-app\-profile\-(.+)#'
                ]
            ],

            'attribute' => [
                'provider' => [
                    // Application profile attribute provider cache configuration
                    'cache' => [
                        /**
                         * Configuration array format:
                         * @see FormatRepository configuration format.
                         */
                        'config' => [
                            'key_pattern' => 'people-app-profile-attr-provider-%s',
                            'key_regexp_select' => '#people\-app\-profile\-attr\-provider\-(.+)#'
                        ]
                    ]
                ]
            ]
        ]
    ]
);