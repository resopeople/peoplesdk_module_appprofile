<?php
/**
 * Description :
 * This class allows to define token module bootstrap class.
 * Token module bootstrap allows to boot token module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_app_profile\token\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\app_profile\token\model\AppProfileTokenKeyEntityFactory;
use people_sdk\module_app_profile\token\library\ConstToken;



class TokenBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Application profile token key entity factory instance.
     * @var AppProfileTokenKeyEntityFactory
     */
    protected $objAppProfileTokenKeyEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param AppProfileTokenKeyEntityFactory $objAppProfileTokenKeyEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        AppProfileTokenKeyEntityFactory $objAppProfileTokenKeyEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objAppProfileTokenKeyEntityFactory = $objAppProfileTokenKeyEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstToken::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set application profile token key entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'app_profile', 'token_key', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objAppProfileTokenKeyEntityFactory->getTabConfig(), $tabConfig);
            $this->objAppProfileTokenKeyEntityFactory->setTabConfig($tabConfig);
        };
    }



}