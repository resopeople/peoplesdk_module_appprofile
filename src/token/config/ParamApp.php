<?php

use people_sdk\app_profile\token\model\AppProfileTokenKeyEntityFactory;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'app_profile' => [
            'token_key' => [
                // Application profile token key entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see AppProfileTokenKeyEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ],

                    /**
                     * Application profile entity factory execution configuration array format:
                     * @see AppProfileTokenKeyEntityFactory::setTabAppProfileEntityFactoryExecConfig() configuration format.
                     */
                    'app_profile_factory_execution_config' => []
                ]
            ]
        ]
    ]
);