<?php

use people_sdk\module_app_profile\token\boot\TokenBootstrap;



return array(
    'people_app_profile_token_bootstrap' => [
        'call' => [
            'class_path_pattern' => TokenBootstrap::class . ':boot'
        ]
    ]
);