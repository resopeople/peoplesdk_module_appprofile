<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\app_profile\app\model\AppProfileEntityFactory;
use people_sdk\app_profile\token\model\AppProfileTokenKeyEntityCollection;
use people_sdk\app_profile\token\model\AppProfileTokenKeyEntityFactory;
use people_sdk\app_profile\token\model\repository\AppProfileTokenKeyEntitySimpleRepository;
use people_sdk\app_profile\token\model\repository\AppProfileTokenKeyEntityMultiRepository;
use people_sdk\app_profile\token\model\repository\AppProfileTokenKeyEntityMultiCollectionRepository;



return array(
    // Application profile token key entity services
    // ******************************************************************************

    'people_app_profile_token_key_entity_collection' => [
        'source' => AppProfileTokenKeyEntityCollection::class
    ],

    'people_app_profile_token_key_entity_factory_collection' => [
        'source' => AppProfileTokenKeyEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_app_profile_token_key_entity_factory' => [
        'source' => AppProfileTokenKeyEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_app_profile_token_key_entity_factory_collection'],
            ['type' => 'class', 'value' => AppProfileEntityFactory::class],
            ['type' => 'config', 'value' => 'people/app_profile/token_key/factory/app_profile_factory_execution_config']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_app_profile_token_key_entity_simple_repository' => [
        'source' => AppProfileTokenKeyEntitySimpleRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_app_profile_token_key_entity_multi_repository' => [
        'source' => AppProfileTokenKeyEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_app_profile_token_key_entity_multi_collection_repository' => [
        'source' => AppProfileTokenKeyEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_app_profile_token_key_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_app_profile_token_key_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Preferences
    // ******************************************************************************

    AppProfileTokenKeyEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_app_profile_token_key_entity_collection']
    ]
);