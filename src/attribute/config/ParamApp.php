<?php

use people_sdk\app_profile\attribute\model\AppProfileAttrEntityFactory;
use people_sdk\app_profile\attribute\provider\model\AppProfileAttrProvider;



return array(
    // People configuration
    // ******************************************************************************

    'people' => [
        'app_profile' => [
            'attribute' => [
                // Application profile attribute entity factory
                'factory' => [
                    /**
                     * Configuration array format:
                     * @see AppProfileAttrEntityFactory configuration format.
                     */
                    'config' => [
                        'select_entity_require' => true,
                        'select_entity_create_require' => true,
                        'select_entity_collection_set_require' => true
                    ]
                ],

                // Application profile attribute provider
                'provider' => [
                    /**
                     * Configuration array format:
                     * Null OR @see AppProfileAttrProvider configuration format.
                     */
                    'config' => []
                ]
            ]
        ]
    ]
);