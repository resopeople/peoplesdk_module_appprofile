<?php

use people_sdk\module_app_profile\attribute\boot\AttributeBootstrap;



return array(
    'people_app_profile_attribute_bootstrap' => [
        'call' => [
            'class_path_pattern' => AttributeBootstrap::class . ':boot'
        ]
    ]
);