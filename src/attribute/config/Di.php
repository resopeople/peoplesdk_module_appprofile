<?php

use liberty_code\di\provider\api\ProviderInterface;
use people_sdk\library\requisition\request\info\factory\api\SndInfoFactoryInterface;
use people_sdk\app_profile\attribute\model\AppProfileAttrEntityCollection;
use people_sdk\app_profile\attribute\model\AppProfileAttrEntityFactory;
use people_sdk\app_profile\attribute\model\repository\AppProfileAttrEntitySimpleRepository;
use people_sdk\app_profile\attribute\model\repository\AppProfileAttrEntitySimpleCollectionRepository;
use people_sdk\app_profile\attribute\model\repository\AppProfileAttrEntityMultiRepository;
use people_sdk\app_profile\attribute\model\repository\AppProfileAttrEntityMultiCollectionRepository;
use people_sdk\app_profile\attribute\provider\model\AppProfileAttrProvider;
use people_sdk\module_app_profile\attribute\helper\AppProfileAttrProviderHelper;



return array(
    // Application profile attribute entity services
    // ******************************************************************************

    'people_app_profile_attr_entity_collection' => [
        'source' => AppProfileAttrEntityCollection::class
    ],

    'people_app_profile_attr_entity_factory_collection' => [
        'source' => AppProfileAttrEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_app_profile_attr_entity_factory' => [
        'source' => AppProfileAttrEntityFactory::class,
        'argument' => [
            ['type' => 'class', 'value' => ProviderInterface::class],
            ['type' => 'dependency', 'value' => 'people_app_profile_attr_entity_collection']
        ],
        'option' => [
            'shared' => true
        ]
    ],

    'people_app_profile_attr_entity_simple_repository' => [
        'source' => AppProfileAttrEntitySimpleRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_app_profile_attr_entity_simple_collection_repository' => [
        'source' => AppProfileAttrEntitySimpleCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_app_profile_attr_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_app_profile_attr_entity_simple_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_app_profile_attr_entity_multi_repository' => [
        'source' => AppProfileAttrEntityMultiRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_requisition_persistor'],
            ['type' => 'class', 'value' => SndInfoFactoryInterface::class]
        ],
        'option' => [
            'shared' => true,
        ]
    ],

    'people_app_profile_attr_entity_multi_collection_repository' => [
        'source' => AppProfileAttrEntityMultiCollectionRepository::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_app_profile_attr_entity_factory'],
            ['type' => 'dependency', 'value' => 'people_app_profile_attr_entity_multi_repository']
        ],
        'option' => [
            'shared' => true,
        ]
    ],



    // Application profile attribute provider services
    // ******************************************************************************

    'people_app_profile_attr_entity_provider_collection' => [
        'source' => AppProfileAttrEntityCollection::class,
        'option' => [
            'shared' => true
        ]
    ],

    'people_app_profile_attr_provider' => [
        'source' => AppProfileAttrProvider::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_app_profile_attr_entity_provider_collection'],
            ['type' => 'config', 'value' => 'people/app_profile/attribute/provider/config'],
            ['type' => 'dependency', 'value' => 'people_app_profile_attr_provider_cache_repository']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Application profile attribute helper services
    // ******************************************************************************

    'people_app_profile_attr_provider_helper' => [
        'source' => AppProfileAttrProviderHelper::class,
        'argument' => [
            ['type' => 'dependency', 'value' => 'people_app_profile_attr_provider'],
            ['type' => 'dependency', 'value' => 'people_app_profile_attr_entity_simple_collection_repository']
        ],
        'option' => [
            'shared' => true
        ]
    ],



    // Preferences
    // ******************************************************************************

    AppProfileAttrEntityCollection::class => [
        'set' => ['type' => 'dependency', 'value' => 'people_app_profile_attr_entity_collection']
    ]
);