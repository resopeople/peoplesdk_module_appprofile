<?php
/**
 * Description :
 * This class allows to define attribute module bootstrap class.
 * Attribute module bootstrap allows to boot attribute module.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_app_profile\attribute\boot;

use liberty_code\framework\bootstrap\model\DefaultBootstrap;

use liberty_code\config\config\api\ConfigInterface;
use liberty_code\framework\application\api\AppInterface;
use liberty_code\framework\framework\config\library\ToolBoxConfig;
use people_sdk\app_profile\attribute\model\AppProfileAttrEntityFactory;
use people_sdk\module_app_profile\attribute\library\ConstAttribute;



class AttributeBootstrap extends DefaultBootstrap
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application configuration instance.
     * @var ConfigInterface
     */
    protected $objAppConfig;



    /**
     * DI: Application profile attribute entity factory instance.
     * @var AppProfileAttrEntityFactory
     */
    protected $objAppProfileAttrEntityFactory;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param ConfigInterface $objAppConfig
     * @param AppProfileAttrEntityFactory $objAppProfileAttrEntityFactory
     */
    public function __construct(
        AppInterface $objApp,
        ConfigInterface $objAppConfig,
        AppProfileAttrEntityFactory $objAppProfileAttrEntityFactory
    )
    {
        // Init properties
        $this->objAppConfig = $objAppConfig;
        $this->objAppProfileAttrEntityFactory = $objAppProfileAttrEntityFactory;

        // Call parent constructor
        parent::__construct($objApp, ConstAttribute::MODULE_KEY);
    }





    // Methods execute
    // ******************************************************************************

    /**
     * Boot module.
     */
    public function boot()
    {
        // Set application profile attribute entity factory configuration, if required
        if(
            is_array($tabConfig = $this->objAppConfig->getValue(
                ToolBoxConfig::getStrPathKeyFromList('people', 'app_profile', 'attribute', 'factory', 'config')
            )) &&
            (count($tabConfig) > 0)
        )
        {
            $tabConfig = array_merge($this->objAppProfileAttrEntityFactory->getTabConfig(), $tabConfig);
            $this->objAppProfileAttrEntityFactory->setTabConfig($tabConfig);
        };
    }



}