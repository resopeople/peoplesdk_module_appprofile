<?php
/**
 * Description :
 * This class allows to define application profile attribute provider helper class.
 * Application profile attribute provider helper allows to provide features,
 * for application profile attribute provider.
 *
 * @copyright Copyright (c) 2018 BOUZID Serge
 * @author Serge BOUZID
 * @version 1.0
 */

namespace people_sdk\module_app_profile\attribute\helper;

use liberty_code\library\bean\model\DefaultBean;

use people_sdk\app_profile\attribute\library\ToolBoxAppProfileAttrRepository;
use people_sdk\app_profile\attribute\provider\model\AppProfileAttrProvider;
use people_sdk\app_profile\attribute\model\repository\AppProfileAttrEntitySimpleCollectionRepository;



class AppProfileAttrProviderHelper extends DefaultBean
{
    // ******************************************************************************
    // Properties
    // ******************************************************************************

    /**
     * Init instances table to dissociate this class from parent
     * @var array
     */
    static protected $__instanceTab = array();



    /**
     * DI: Application profile attribute provider instance.
     * @var AppProfileAttrProvider
     */
    protected $objAppProfileAttrProvider;



    /**
     * DI: Application profile attribute entity collection repository instance.
     * @var AppProfileAttrEntitySimpleCollectionRepository
     */
    protected $objAppProfileAttrEntityCollectionRepo;





    // ******************************************************************************
    // Methods
    // ******************************************************************************

    // Constructor / Destructor / Others
    // ******************************************************************************

    /**
     * @inheritdoc
     * @param AppProfileAttrProvider $objAppProfileAttrProvider
     * @param AppProfileAttrEntitySimpleCollectionRepository $objAppProfileAttrEntityCollectionRepo
     */
    public function __construct(
        AppProfileAttrProvider $objAppProfileAttrProvider,
        AppProfileAttrEntitySimpleCollectionRepository $objAppProfileAttrEntityCollectionRepo
    )
    {
        // Init properties
        $this->objAppProfileAttrProvider = $objAppProfileAttrProvider;
        $this->objAppProfileAttrEntityCollectionRepo = $objAppProfileAttrEntityCollectionRepo;

        // Call parent constructor
        parent::__construct();
    }





    // Methods setters
    // ******************************************************************************

    /**
     * Reset application profile attribute provider.
     */
    public function reset()
    {
        // Clear cache, if required
        if(!is_null($objCacheRepo = $this->objAppProfileAttrProvider->getObjCacheRepo()))
        {
            $objCacheRepo->removeTabItem($objCacheRepo->getTabSearchKey());
        };

        // Clear collection
        $objAppProfileAttrEntityCollection = $this->objAppProfileAttrProvider->getObjAttributeCollection();
        $objAppProfileAttrEntityCollection->removeItemAll();
    }





    // Methods repository
    // ******************************************************************************

    /**
     * Load application profile attributes, to get specified schemas,
     * available for application profile attribute provider.
     * Return true if success, false if an error occurs.
     *
     * Application profile attribute entity collection repository execution configuration array format:
     * @see ToolBoxAppProfileAttrRepository::loadSchema() configuration array format.
     *
     * @param boolean $boolSchemaRequired = false
     * @param boolean $boolSchemaPrivateGetRequired = false
     * @param boolean $boolSchemaPrivateUpdateRequired = false
     * @param boolean $boolSchemaPublicGetRequired = false
     * @param boolean $boolSchemaPublicExtendGetRequired = false
     * @param null|array $tabAppProfileAttrEntityCollectionRepoExecConfig = null
     * @return boolean
     */
    public function loadSchema(
        $boolSchemaRequired = false,
        $boolSchemaPrivateGetRequired = false,
        $boolSchemaPrivateUpdateRequired = false,
        $boolSchemaPublicGetRequired = false,
        $boolSchemaPublicExtendGetRequired = false,
        array $tabAppProfileAttrEntityCollectionRepoExecConfig = null
    )
    {
        // Reset provider
        $this->reset();

        // Return result
        return ToolBoxAppProfileAttrRepository::loadSchema(
            $this->objAppProfileAttrProvider->getObjAttributeCollection(),
            $this->objAppProfileAttrEntityCollectionRepo,
            $boolSchemaRequired,
            $boolSchemaPrivateGetRequired,
            $boolSchemaPrivateUpdateRequired,
            $boolSchemaPublicGetRequired,
            $boolSchemaPublicExtendGetRequired,
            $tabAppProfileAttrEntityCollectionRepoExecConfig
        );
    }



}