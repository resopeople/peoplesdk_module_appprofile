<?php

// Init var
$strRootPath = dirname(__FILE__) . '/..';

// Include class
include($strRootPath . '/src/attribute/library/ConstAttribute.php');
include($strRootPath . '/src/attribute/helper/AppProfileAttrProviderHelper.php');
include($strRootPath . '/src/attribute/boot/AttributeBootstrap.php');

include($strRootPath . '/src/app/library/ConstApp.php');
include($strRootPath . '/src/app/helper/AppProfileFactoryHelper.php');
include($strRootPath . '/src/app/boot/AppBootstrap.php');

include($strRootPath . '/src/api/library/ConstApi.php');
include($strRootPath . '/src/api/boot/ApiBootstrap.php');

include($strRootPath . '/src/token/library/ConstToken.php');
include($strRootPath . '/src/token/boot/TokenBootstrap.php');

include($strRootPath . '/src/requisition/library/ConstRequisition.php');

include($strRootPath . '/src/cache/library/ConstCache.php');